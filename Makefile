hlint:
	(cd support; ~/.cabal/bin/hlint `find ../src -name \*.hs`)

clean-sandbox:
	- cabal sandbox hc-pkg unregister MuCheck-HUnit

sandbox:
	mkdir -p ../mucheck-sandbox
	cabal sandbox init --sandbox ../mucheck-sandbox

build:
	cabal sandbox init --sandbox ../mucheck-sandbox
	cabal build

run:
	cabal sandbox init --sandbox ../mucheck-sandbox
	- rm *.tix
	cabal build sample-test
	./dist/build/sample-test/sample-test
	env MuDEBUG=1 ./dist/build/mucheck-hunit/mucheck-hunit -tix sample-test.tix Examples/HUnitTest.hs

prepare:
	cabal haddock
	cabal check
	cabal sdist

clean:
	- rm Examples/*_*
	- rm *.log

install:
	cabal sandbox init --sandbox ../mucheck-sandbox
	cabal install

